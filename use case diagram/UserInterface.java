package usecasediagram;

import java.util.List;

import classdiagram.*;

/**
 * About the user's operation
 /**
  * 
  * @plant.uml
  * @plant.uml admin -up>(UserInterface) : <<uses>>
  */
public interface UserInterface {
	/**
	 * add users
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface) ->(addUser) : <<extends>>
	 * 
	 */
	public int addUser(String[] args);

	/**
	 * delete users
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * @plant.uml (UserInterface) ->(deleteUser) :<<extends>>
	 */
public int deleteUser(String[] args);

	/**
	 * update users
	 * 
	 * @param args
	 * @return
	 */
/**
 * 
 * @plant.uml (UserInterface)->(updateUserInfo) :<<extends>>
 * 
 */
	public int updateUserInfo(String[] args);

	/**
	 * Get user profile
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(getUser) :<<extends>>
	 * 
	 */
	public UserInfoBean getUser(String[] args);

	/**
	 * Check login
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(checkLogin) :<<extends>>
	 * 
	 */
	public UserInfoBean checkLogin(String[] args);

	/**
	 * Recharge
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(rechargeUser) :<<extends>>
	 * 
	 */
	public int rechargeUser(String[] args);

	/**
	 * updatePsw
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(updatePsw) :<<extends>>
	 * 
	 */
	public int updatePsw(String[] args);

	/**
	 * checkAdminLogin
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(checkAdminLogin) :<<extends>>
	 * 
	 */
	public AdminBean checkAdminLogin(String[] args);

	/**
	 * getAllUser
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(getAllUser) :<<extends>>
	 * 
	 */
	public List<UserInfoBean> getAllUser(String[] args);

	/**
	 * addArrearageUser
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(addArrearageUser) :<<extends>>
	 * 
	 */
	public int addArrearageUser(String[] args);

	/**
	 * getArrearageUser
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(getArrearageUser) :<<extends>>
	 * 
	 */
	public BlackListBean getArrearageUser(String[] args);

	/**
	 * deleteArrearageUser
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (UserInterface)->(deleteArrearageUser) :<<extends>>
	 * 
	 */
	public int deleteArrearageUser(String[] args);
}