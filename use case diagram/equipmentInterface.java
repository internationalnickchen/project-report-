package usecasediagram;

/**
 * About the operation of the equipment
 */
/**
 * 
 * @plant.uml actor admin
 * @plant.uml admin -right> (equipmentInterface) :<<uses>>
 *
 */
public interface equipmentInterface {
	/**
	 * Add equipment type
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (addKindsequipments):<<extends>>
	 */
	public int addKindsequipments(String[] args);

	/**
	 * Update equipment type
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (updateKindsequipments):<<extends>>
	 */
	public int updateKindsequipments(String[] args);

	/**
	 * Delete equipment type
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (deleteKindsequipments):<<extends>>
	 */
	public int deleteKindsequipments(String[] args);

	/**
	 * add equipments
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (addequipments):<<extends>>
	 */
	public int addequipments(String[] args);

	/**
	 * delete equipments
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (deleteequipments):<<extends>>
	 */
	public int deleteequipments(String[] args);

	/**
	 * Borrowing equipment
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (borrowequipment):<<extends>>
	 */
	public int borrowequipment(String[] args);

	/**
	 * Return equipment
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (backequipmen):<<extends>>
	 */
	public int backequipment(String[] args);

	/**
	 * Renewal of equipments
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (renewequipment):<<extends>>
	 */
	public int renewequipment(String[] args);
	/**
	 * Delete books in the new equipments list
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (deleteNewequipment):<<extends>>
	 */
	public int deleteNewequipment(String[] args);
	/**
	 * Add new equipment inventory
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (addNewStocks):<<extends>>
	 */
	public int addNewStocks(String[] args);
	/**
	 * Delete equipment inventory
	 * 
	 * @param args
	 * @return
	 */
	/**
	 * 
	 * @plant.uml (equipmentInterface) -> (delNewStocks):<<extends>>
	 */
	public int delNewStocks(String[] args);

}