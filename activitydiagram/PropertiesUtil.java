package activitydiagram;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 
 * @plant.uml split again
 * @plant.uml :PropertiesUtil;
 *
 */
public final class PropertiesUtil {
/**
 * @plant.uml if(propertiesUtil =null) then (yes)
 * :properties =new properties;
 * else (No)
 * :throw;
 */
	private static PropertiesUtil util = null;
	private Properties prop = new Properties(); 

	/**
	 * 
	 */
	private PropertiesUtil() {
		try {
			load();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void load() throws IOException {
		// 
		// InputStream is = new FileInputStream("dbconfig.properties");
		// 
		InputStream is = this.getClass().getResourceAsStream(
				"dbconfig.properties");
		prop.load(is);
	}

	/**
	 * 
	 * 
	 * @param key
	 * @return
	 */
	/**
	 * 
	 * @plant.uml : get key;
	 * @stop
	 */
	public static String get(String key) {
		if (util == null)
			util = new PropertiesUtil();
		return util.prop.getProperty(key);
	}
}