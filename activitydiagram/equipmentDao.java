package activitydiagram;

import usecasediagram.equipmentInterface;

/**
 * 
 * @plant.uml split again
 * @plant.uml :equipmentDao;
 * @plant.uml :BaseDao;
 *
 */
public class equipmentDao extends BaseDao implements equipmentInterface {


	@Override
	public int addKindsequipments(String[] args) {
		// TODO 
		String sql = "insert into tb_equipmenttype (btid, type, days) values (?,?,'"
				+ super.getDay() + "')";
		return super.update(sql, args);
	}

	@Override
	public int updateKindsequipments(String[] args) {
		// TODO 
		String sql = "update tb_equipmenttype set type=? where btid=?";
		// System.out.println(args[0]+"*******"+args[1]);
		return super.update(sql, args);
	}

	/**
	 * @plant.uml :addequipments;
	 */
	@Override
	public int addequipments(String[] args) {
		// TODO 
		String sql = "insert into tb_equipmentinfo (ISBN, typeId, bookname, date, price) values (?,?,?,?,?,?,?,?)";
		return super.update(sql, args);
	}

	/**
	 * @plant.uml : deletequipments;
	 */
	@Override
	public int deleteequipments(String[] args) {
		// TODO 
		String sql = "delete from tb_equipmentinfo where ISBN=?";
		return super.update(sql, args);
	}

	/**
	 * @plant.uml : borrowequipment;
	 */
	@Override
	public int borrowequipment(String[] args) {
		String sql1 = "insert into tb_borrowrecord (equipmentID, userID, borrowDate, backDate, isback) values (?,?,'"
				+ super.getDate() + "','" + super.getAfter30Date() + "',0)";
		String sql2 = "update tb_stockpile set amount=amount-1 where ISBN=?";
		int n = super.update(sql1, args);
		String arg[] = { args[0] };
		int m = super.update(sql2, arg);// args[0]
		return n & m;
	}


	/**
	 * @plant.uml : backequipment;
	 */
	@Override
	public int backequipment(String[] args) {
		// TODO 
		System.out.println(super.getDate());
		String sql1 = "update tb_borrowrecord set isback=1,backDate='"
				+ super.getDate() + "' where bookISBN=? and userISBN=?";
		String sql2 = "update tb_stockpile set amount=amount+1 where ISBN=?";
		System.out.println(sql1);
		int n = super.update(sql1, args);
		String args2[] = { args[0] };
		int m = super.update(sql2, args2);
		return n & m;
	}

	/**
	 * @plant.uml :renwequipment;
	 */
	@Override
	public int renewequipment(String[] args) {
		// TODO 
		System.out.println(super.getDate());
		String sql1 = "update tb_borrowrecord set backDate='"
				+ super.getAfter30Date() + "',borrowDate='"+super.getDate()+"' where bookISBN=? and userISBN=?";
		System.out.println(sql1);
		int n = super.update(sql1, args);
		return n;
	}
	/**
	 * @plant.uml :deleteNweeuqipment;
	 */
	@Override
	public int deleteNewequipment(String[] args) {
		// TODO 
		String sql = "delete from tb_newbooks where newISBN=?";
		return super.update(sql, args);
	}
	/**
	 *@plant.uml : addNweStocks;
	 */
	@Override
	public int addNewStocks(String[] args) {
		// TODO 
		String sql = "insert into tb_stockpile(ISBN,amount) values(?,5)";
		return super.update(sql, args);
	}
	/**
	 *@plant.uml : delNewStocks;
	 */
	@Override
	public int delNewStocks(String[] args) {
		// TODO 
		String sql = "delete from tb_stockpile where ISBN=?";
		return super.update(sql, args);
	}
/**
 * @plant.uml : deleteKindsequipments
 * @plant.uml stop
 */
	@Override
	public int deleteKindsequipments(String[] args) {
		// TODO Auto-generated method stub
		return 0;
	}

}