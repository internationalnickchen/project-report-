package activitydiagram;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import classdiagram.*;
import usecasediagram.UserInterface;

/**
 * @plant.uml split 
 * @plant.uml :USerDao;
 * @plant.uml split;
 * @plant.uml :BaseDao;
 * @plant.uml :UserInterface;
 */
public class UserDao extends BaseDao implements UserInterface {

	/**
	 * add users
	 */
	@Override
	public int addUser(String[] args) {
		// TODO 
		String sql = "insert into tb_user (Uname, Uage, UidCard, effectivedate, Utel, balance, UserId,starttime,Upsw) values (?,?,?,'"
				+ super.getAfterDate() + "',?,0,?,'" + super.getDate() + "',?)";
		return super.update(sql, args);
	}

	/**
	 * add BlackList
	 */
	/**
	 * plant.uml :add ArrearageUser;
	 */
	@Override
	public int addArrearageUser(String[] args) {
		// TODO 
		String sql = "insert into tb_blacklist (arrearageName, arrearageId, arrearageIdCard,arrearageBalance) values (?,?,?,?)";
		return super.update(sql, args);
	}


	/**
	 *delete users
	 */
	/*
	/**
	 * update 
	 */
	/**
	 * @plant.uml :update;
	 */
	@Override
	public int updateUserInfo(String[] args) {
		// TODO 
		String sql = "update tb_user set Uname=?,Utel=? where UserId=?";
		return super.update(sql, args);
	}


	/**
	 * getUser
	 */
	/**
	 * @plant.uml :getUser;
	 */
	@Override
	public UserInfoBean getUser(String[] args) {
		// TODO 
		String sql = "select Uname,Uage,UidCard,effectivedate,Utel,balance,startTime,Upsw from tb_user where UserId=?";
		ResultSet rs = super.select(sql, args);
		UserInfoBean uib = null;
		try {
			if (rs.next()) {
				String uname = rs.getString("Uname");
				String uage = String.valueOf(rs.getInt("Uage"));
				String uidCard = rs.getString("UidCard");
				String effectivedate = rs.getDate("effectivedate").toString();
				String utel = rs.getString("Utel");
				String balance = String.valueOf(rs.getFloat("balance"));
				String startTime = rs.getDate("startTime").toString();
				String upsw = rs.getString("Upsw");
				uib = new UserInfoBean(uname, uage, uidCard, effectivedate,
						utel, balance, args[0], startTime, upsw);
				return uib;
			}
			/**
			 * @plant.uml else (No)
			 * @plant.uml :catch 
			 */
		} catch (SQLException e) {
			// TODO 
			e.printStackTrace();
			return uib;
		}
		return uib;
	}

	/*

	/**
	 *check
	 */
	@Override
	public UserInfoBean checkLogin(String[] args) {
		// TODO 
		String sql = "select Uname,Uage,UidCard,effectivedate,Utel,balance,starttime from tb_user where UserId=? and Upsw=?";
		ResultSet rs = super.select(sql, args);
		UserInfoBean uib = null;
		try {
			if (rs.next()) {
				String uname = rs.getString("Uname");
				String uage = String.valueOf(rs.getInt("Uage"));
				String uidCard = rs.getString("UidCard");
				String effectivedate = rs.getDate("effectivedate").toString();
				String utel = rs.getString("Utel");
				String balance = String.valueOf(rs.getFloat("balance"));
				String startTime = rs.getDate("startTime").toString();
				uib = new UserInfoBean(uname, uage, uidCard, effectivedate,
						utel, balance, args[0], startTime, args[1]);
				return uib;
			}
		} catch (SQLException e) {
			// TODO 
			e.printStackTrace();
			return uib;
		}
		return uib;
	}

	/**
	 * check login
	 */
	@Override
	public AdminBean checkAdminLogin(String[] args) {
		// TODO 
		String sql = "select Aname,Asex,Aage,AidCard,Atel,Alevel from tb_admin where Aid=? and Apsw=?";
		ResultSet rs = super.select(sql, args);
		AdminBean ab = null;
		try {
			if (rs.next()) {
				String Aname = rs.getString("Aname");
				String Asex = rs.getString("Asex");
				String Aage = rs.getString("Aage");
				String AidCard = rs.getString("AidCard");
				String Atel = rs.getString("Atel");
				String Alevel = rs.getString("Alevel");
				ab = new AdminBean(args[0], Aname, Asex, Aage, AidCard, Atel,
						Alevel, args[1]);
				return ab;
			}
		} catch (SQLException e) {
			// TODO 
			e.printStackTrace();
			return null;
		}
		return ab;
	}
   
	/**
	 *@plant.uml : recharge;
	 */ 
	@Override
	public int rechargeUser(String[] args) {
		// TODO Auto-generated method stub
		String sql = "update tb_user set balance=balance+? where UserId=?";
		return super.update(sql, args);
	}
    
	/**
	 *@plant.uml :updatepsw;
	 */
	@Override
	public int updatePsw(String[] args) {
		// TODO Auto-generated method stub
		String sql = "update tb_user set Upsw=? where UserId=?";
		return super.update(sql, args);
	}

	/**
	 * @plant.uml while (rs.next)
	 * @plant.uml :add;
	 * @plant.uml endwhile
	 * 
	 */
	@Override
	public List<UserInfoBean> getAllUser(String[] args) {
		// TODO Auto-generated method stub
		String sql = "select Uname,Uage,UidCard,effectivedate,Utel,balance,UserId,startTime,Upsw from tb_user";
		ResultSet rs = super.select(sql, args);
		List<UserInfoBean> uib = new ArrayList<UserInfoBean>();
		try {
			while (rs.next()) {
				String uname = rs.getString("Uname");
				String uage = String.valueOf(rs.getInt("Uage"));
				String uidCard = rs.getString("UidCard");
				String effectivedate = rs.getDate("effectivedate").toString();
				String utel = rs.getString("Utel");
				String balance = String.valueOf(rs.getFloat("balance"));
				String startTime = rs.getDate("startTime").toString();
				String upsw = rs.getString("Upsw");
				String UserId = rs.getString("UserId");
				uib.add(new UserInfoBean(uname, uage, uidCard, effectivedate,
						utel, balance, UserId, startTime, upsw));
			}
		} catch (SQLException e) {
			// TODO 
			e.printStackTrace();
		}
		return uib;
	}

	/**
	 * @plant.uml :getarrearageUser;
	 */
	@Override
	public BlackListBean getArrearageUser(String[] args) {
		// TODO 
		String sql = "select arrearageName,arrearageIdCard,arrearageBalance from tb_blacklist where arrearageId=?";
		ResultSet rs = super.select(sql, args);
		BlackListBean uib = null;
		try {
			if (rs.next()) {
				String arrearageName = rs.getString("arrearageName");
				String arrearageIdCard = rs.getString("arrearageIdCard");
				String arrearageBalance = rs.getString("arrearageBalance");
				uib = new BlackListBean(arrearageName, args[0],
						arrearageIdCard, arrearageBalance);
				return uib;
			}
		} catch (SQLException e) {
			// TODO 
			e.printStackTrace();
			return uib;
		}
		return uib;
	}

	/**
	 * @plant.uml :deleteArrearageUser;
	 */
	@Override
	public int deleteArrearageUser(String[] args) {
		// TODO 
		String sql = "delete from tb_blacklist where arrearageId=?";
		return super.update(sql, args);
	}

	/**
	 * @plant.uml :rechargeArrearageUser;
	 */
	public int rechargeArrearageUser(String[] args) {
		// TODO Auto-generated method stub
		String sql = "update tb_blacklist set arrearageBalance=arrearageBalance+? where arrearageId=?";
		return super.update(sql, args);
	}
/**
 * @plant.uml stop
 */
	@Override
	public int deleteUser(String[] args) {
		// TODO Auto-generated method stub
		return 0;
	}
}