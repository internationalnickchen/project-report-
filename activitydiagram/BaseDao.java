package activitydiagram;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import classdiagram.equipmentInfoBean;

/**
 * add,reduce.etc.
 */
/**
 * 
 * @plant.uml start
 * @plant.uml :basedao;
 *
 */
public class BaseDao {
	private String driver = "";
	private String url = "";
	private String user = "";
	private String password = "";
	private static int time = 2;
	private static int day = 30;
/**
 *
 */
	public BaseDao() {
		driver = PropertiesUtil.get("driver");
		url = PropertiesUtil.get("url");
		user = PropertiesUtil.get("user");
		password = PropertiesUtil.get("password");
	}
/**
 * 
 * @plant.uml :connection;
 */
	public final Connection getConnection() {
		try {
			Class.forName(driver);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * 
	 * 
	 * @param sql
	 *           
	 * @param args
	 *            
	 * @return
	 */
	/**
	 * 
	 * @plant.uml :update;
	 */
	public int update(String sql, String[] args) {
		try {
			PreparedStatement pstmt = getConnection().prepareStatement(sql);
			// System.out.println(args[0]+"**&&&**"+args[1]);
			if (args != null && args.length > 0) {
				for (int i = 0; i < args.length; i++) {
					pstmt.setString(i + 1, args[i]);
				}
			}
			int result = pstmt.executeUpdate();
			pstmt.close();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return -1;
		}
	}

	/**
	 * 
	 * @plant.uml :select;
	 */
	public ResultSet select(String sql, String[] args) {
		try {
			PreparedStatement pstmt = getConnection().prepareStatement(sql);
			if (args != null && args.length > 0) {
				for (int i = 0; i < args.length; i++) {
					pstmt.setString(i + 1, args[i]);
				}
			}
			ResultSet result = pstmt.executeQuery();
			return result;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 
	 * 
	 * @return
	 */
	/**
	 * 
	 * @plant.uml :getDate;
	 */
	public String getDate() {
		return new SimpleDateFormat("yyyy-MM-dd").format(new Date()).toString()
				.trim();
	}

	/**
	 * 
	 * 
	 * @return
	 */
	/**
	 * 
	 * @plant.uml :getAfterDate;
	 */
	public String getAfterDate() {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(new Date());
		gc.add(1, time);
		return sf.format(gc.getTime());
	}

	/**
	 * 
	 * 
	 * @return
	 */
	/**
	 * 
	 * @plant.uml :getAfter30Date;
	 */
	public String getAfter30Date() {
		GregorianCalendar gc = new GregorianCalendar();
		SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		gc.setTime(new Date());
		gc.add(5, day);
		return sf.format(gc.getTime());
	}

	/**
	 * @return day;
	 */
	/**
	 * 
	 * @plant.uml :getDay;
	 */
	public static int getDay() {
		return day;
	}

	/**
	 * @param day
	 *            
	 */
	/**
	 * 
	 * @plant.uml :setDay;
	 */
	public static void setDay(int day) {
		BaseDao.day = day;
	}
	/**
	 * 
	 * @plant.uml :addKindsequipments;
	 */
	public int addKindsequipments(String[] args) {
		// TODO Auto-generated method stub
		return 0;
	}
	/**
	 * 
	 * @plant.uml :QueryKindsequipment;
	 */
	public List<equipmentInfoBean> QueryKindsequipment(String[] args) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * 
	 * @plant.uml :QuerySearchequipment;
	 * @plant.uml stop
	 */
	public List<equipmentInfoBean> QuerySearchequipment(String[] args, String[] a) {
		// TODO Auto-generated method stub
		return null;
	}
}