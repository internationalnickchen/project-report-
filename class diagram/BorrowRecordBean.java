package classdiagram;

/**
 * BorrowRecordJavaBean
 */
/**
 * 
 * @plant.uml
 * @plant.uml class BorrowRecord{
 * @plant.uml String recordid;
 * @plant.uml String equipmentID;
 * @plant.uml String userID;
 * @plant.uml String isback;
 * @plant.uml String borrowDate;
 * @plant.uml String backDate;
 * @plant.uml String equipmentName;
 * @plant.uml }
 */
public class BorrowRecordBean {
	private String recordid;
	private String equipmentID;
	private String userID;
	private String isback;
	private String borrowDate;
	private String backDate;
	private String equipmentName;
/**
 * 
 * @plant.uml BorrowRecord : String getequipmentName()
 */
	public String getequipmentName() {
		return equipmentName;
	}
/**
 * 
 * @plant.uml BorrowRecord : void setequipmentName(String equipmentName)
 */
	public void setequipmentName(String equipmentName) {
		this.equipmentName = equipmentName;
	}
/**
 * @plant.uml BorrowRecord : BorrowRecordBean()
 */
	public BorrowRecordBean() {
		super();
	}

	/**
	 * @param recordid
	 * @param equipmentID
	 * @param adminId
	 * @param userID
	 * @param isback
	 * @param borrowDate
	 * @param backDate
	 */
	/**
	 * 
	 * @param recordid
	 * @param equipmentID
	 * @param equipmentName
	 * @param userID
	 * @param isback
	 * @param borrowDate
	 * @param backDate
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord : BorrowRecordBean(String recordid, String equipmentID, String equipmentName,
			String userID,String isback, String borrowDate, String backDate)
	 */
	public BorrowRecordBean(String recordid, String equipmentID, String equipmentName,
			String userID,String isback, String borrowDate, String backDate) {
		super();
		this.recordid = recordid;
		this.equipmentID = equipmentID;
		this.equipmentName = equipmentName;
		this.userID = userID;
		this.isback = isback;
		this.borrowDate = borrowDate;
		this.backDate = backDate;
	}

	/**
	 * @return recordid
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord : String getRecordid()
	 */
	public String getRecordid() {
		return recordid;
	}

	/**
	 * @param recordid
	 *      set    recordid
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  void setRecordid(String recordid)
	 */
	public void setRecordid(String recordid) {
		this.recordid = recordid;
	}

	/**
	 * @return equipmentID
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord : String getequipmentID()
	 */
	public String getequipmentID() {
		return equipmentID;
	}

	/**
	 * @param equipmentID
	 *     set     equipmentID
	 */
	/**
	 * 
	 * @plant.uml  BorrowRecord :  void setequipmentIDN(String equipmentID) 
	 */
	public void setequipmentIDN(String equipmentID) {
		this.equipmentID = equipmentID;
	}

	/**
	 * @return userID
	 */
	/**
	 * @plant.uml  BorrowRecord : String getuserID()
	 */
	public String getuserID() {
		return userID;
	}

	/**
	 * @param userISBN
	 *   set       serID
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord : void setUserISBN(String userID)
	 */
	public void setUserISBN(String userID) {
		this.userID = userID;
	}

	/**
	 * @return isback
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord : String getIsback()
	 */
	public String getIsback() {
		return isback;
	}

	/**
	 * @param isback
	 *     set     isback
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  void setIsback(String isback)
	 */
	public void setIsback(String isback) {
		this.isback = isback;
	}

	/**
	 * @return borrowDate
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  String getBorrowDate()
	 */
	public String getBorrowDate() {
		return borrowDate;
	}

	/**
	 * @param borrowDate
	 *     set    borrowDate
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  void setBorrowDate(String borrowDate)
	 */
	public void setBorrowDate(String borrowDate) {
		this.borrowDate = borrowDate;
	}

	/**
	 * @return backDate
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  String getBackDate()
	 */
	public String getBackDate() {
		return backDate;
	}

	/**
	 * @param backDate
	 *    set      backDate
	 */
	/**
	 * 
	 * @plant.uml BorrowRecord :  void setBackDate(String backDate)
	 */
	public void setBackDate(String backDate) {
		this.backDate = backDate;
	}
}