package classdiagram;

/**
 * equipmentinfoJavaBean
 */
/**
 * 
 * @plant.uml  equipmentInfoBean "1" -- "1"equipmentTypeBean
 * @plant.uml  class equipmentInfoBean{
 * @plant.uml  String equipmentID;
 * @plant.uml  String typeId;
 * @plant.uml  String equipmentname;
 * @plant.uml  String price;
 * @plant.uml }
 */
public class equipmentInfoBean {
	/**
	 * equipmentID
	 */
	private String equipmentID;
	/**
	 * equipmenttype
	 */
	private String typeId;
	/**
	 * equipmentname
	 */
	private String equipmentname;
	/**
	 * changdiprice
	 */
	private String price;

	/**
	 * @param price
	 * @param date 
	 * @param equipmentname
	 * @param args 
	 * @param iD 
	 * 
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : equipmentInfoBean(String iD, String args, String equipmentname, String date, String price)
	 * 
	 */
	public equipmentInfoBean(String iD, String args, String equipmentname, String date, String price) {
		super();
	}

	/**
	 * @param equipmentID
	 * @param typeId
	 * @param equipmentname
	 * @param price
	 */
	public equipmentInfoBean(String equipmentID, String typeId, String equipmentname,
			String price) {
		super();
		this.equipmentID = equipmentID;
		this.typeId = typeId;
		this.equipmentname = equipmentname;
		this.price = price;
	}

	/**
	 * @return equipmentID
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : String getequipmentID
	 * 
	 */
	public String getequipmentID() {
		return equipmentID;
	}

	/**
	 * @param equipmentID
	 *            set equipmentID
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : void setequipmentID(String equipmentID)
	 * 
	 */
	public void setequipmentID(String equipmentID) {
		this.equipmentID = equipmentID;
	}

	/**
	 * @return typeId
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean :  String getTypeId()
	 */
	public String getTypeId() {
		return typeId;
	}

	/**
	 * @param typeId
	 *            set typeId
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : void setTypeId(String typeId) 
	 */
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return equipmentname
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : String getequipmentname()
	 */
	public String getequipmentname() {
		return equipmentname;
	}

	/**
	 * @param bookname
	 *            set equipmentname
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : void setequipmentname(String equipmentname)
	 */
	public void setequipmentname(String equipmentname) {
		this.equipmentname = equipmentname;
	}
	/**
	 * @return price
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : String getPrice()
	 */
	public String getPrice() {
		return price;
	}

	/**
	 * @param price
	 *            set price
	 */
	/**
	 * 
	 * @plant.uml equipmentInfoBean : void setPrice(String price)
	 */
	public void setPrice(String price) {
		this.price = price;
	}
	/**
	 * 
	 * @plant.uml equipmentInfoBean : void setDate(String date)
	 */
	public void setDate(String date) {
		// TODO Auto-generated method stub
		
	}
}