package classdiagram;

/**
 * management system staff JavaBean
 */
/**
 * 
 * @plant.uml AdminBean"1" <-- "many" equipmentInfoBean
 * @plant.uml AdminBean"1" <-- "many" BorrowRecord
 * @plant.uml AdminBean"1" -- "many" UserInfoBean
 * @plant.uml class AdminBean{
 * @plant.uml String aid;
 * @plant.uml String.aname;
 * @plant.uml String.asex;
 * @plant.uml String aage;
 * @plant.uml String aage;
 * @plant.uml String aidCard;
 * @plant.uml String atel;
 * @plant.uml String alevel;
 * @plant.uml String apsw;
 * @plant.uml }
 */
public class AdminBean {
	private String aid;
	private String aname;
	private String asex;
	private String aage;
	private String aidCard;
	private String atel;
	private String alevel;
	private String apsw;
/**
 * @plant.uml AdminBean : Adminbean()
 */
	public AdminBean() {
		super();
	}

	/**
	 * @param aid
	 * @param aname
	 * @param asex
	 * @param aage
	 * @param aidCard
	 * @param atel
	 * @param alevel
	 * @param apsw
	 */
	/**
	 * @plant.uml AdminBean : Adminbean(String aid,String aname,String asex,String aage,String aidCard, String atel, String alevel, String apsw)
	 */
	public AdminBean(String aid, String aname, String asex, String aage,
			String aidCard, String atel, String alevel, String apsw) {
		super();
		this.aid = aid;
		this.aname = aname;
		this.asex = asex;
		this.aage = aage;
		this.aidCard = aidCard;
		this.atel = atel;
		this.alevel = alevel;
		this.apsw = apsw;
	}

	/**
	 * @return aid
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAid()
	 */
	public String getAid() {
		return aid;
	}

	/**
	 * @param aid
	 *            seted aid
	 */
	/**
	 * 
	 * @plant.uml 
	 * @plant.uml AdminBean : void setAid(String aid)
	 */
	public void setAid(String aid) {
		this.aid = aid;
	}

	/**
	 * @return aname
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAname()
	 */
	public String getAname() {
		return aname;
	}

	/**
	 * @param aname
	 *            seted aname
	 */
	/**
	 * 
	 * @plant.uml AdminBean : void setAname(String aname)
	 */
	public void setAname(String aname) {
		this.aname = aname;
	}

	/**
	 * @return asex
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAsex()
	 */
	public String getAsex() {
		return asex;
	}

	/**
	 * @param asex
	 *            seted asex
	 */
	/**
	 * 
	 * @plant.uml AdminBean : void setAsex(String asex)
	 */
	public void setAsex(String asex) {
		this.asex = asex;
	}

	/**
	 * @return aage
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String get Aage()
	 */
	public String getAage() {
		return aage;
	}

	/**
	 * @param aage
	 *            seted aage
	 */
	/**
	 * 
	 * @plant.uml AdminBean : void setAage(String aage)
	 */
	public void setAage(String aage) {
		this.aage = aage;
	}

	/**
	 * @return aidCard
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAidCard()
	 */
	public String getAidCard() {
		return aidCard;
	}

	/**
	 * @param aidCard
	 *            seted aidCard
	 */
	/**
	 * 
	 * @plant.uml AdminBean : setAidCard(String aidCard)
	 */
	public void setAidCard(String aidCard) {
		this.aidCard = aidCard;
	}

	/**
	 * @return atel
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAtel()
	 */
	public String getAtel() {
		return atel;
	}

	/**
	 * @param atel
	 *            seted atel
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String atel
	 */
	public void setAtel(String atel) {
		this.atel = atel;
	}

	/**
	 * @return alevel
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getAlevel()
	 */
	public String getAlevel() {
		return alevel;
	}

	/**
	 * @param alevel
	 *            seted alevel
	 */
	/**
	 * 
	 * @plant.uml AdminBean : void setAlevel(String alevel)
	 */
	public void setAlevel(String alevel) {
		this.alevel = alevel;
	}

	/**
	 * @return apsw
	 */
	/**
	 * 
	 * @plant.uml AdminBean : String getApsw()
	 */
	public String getApsw() {
		return apsw;
	}

	/**
	 * @param apsw
	 *            seted apsw
	 */
	/**
	 * 
	 * @plant.uml AdminBean : void setApsw(String apsw)
	 */
	public void setApsw(String apsw) {
		this.apsw = apsw;
	}
}