package classdiagram;

/**
 * UserinformationJavaBean
 */
/**
 * 
 * @plant.uml  class UserInfoBean{
 * @plant.uml  String uname;
 * @plant.uml  String uage;
 * @plant.uml  String uidCard;
 * @plant.uml  String effectivedate;
 * @plant.uml  String utel;
 * @plant.uml  String balance;
 * @plant.uml  String userId;
 * @plant.uml  String startTime;
 * @plant.uml  String upsw;
 * @plant.uml }
 */
public class UserInfoBean {
	private String uname;
	private String uage;
	private String uidCard;
	private String effectivedate;
	private String utel;
	private String balance;
	private String userId;
	private String startTime;
	private String upsw;
/**
 *@plant.uml  UserInfoBean : UserInfoBean()
 */
	public UserInfoBean() {
		super();
	}

	/**
	 * @param uname
	 * @param uage
	 * @param uidCard
	 * @param effectivedate
	 * @param utel
	 * @param balance
	 * @param userId
	 * @param startTime
	 * @param upsw
	 */
	/**
	 *@plant.uml  UserInfoBean : UserInfoBean(String uname, String uage, String uidCard,
			String effectivedate, String utel, String balance, String userId,
			String startTime, String upsw) 
	 */
	public UserInfoBean(String uname, String uage, String uidCard,
			String effectivedate, String utel, String balance, String userId,
			String startTime, String upsw) {
		super();
		this.uname = uname;
		this.uage = uage;
		this.uidCard = uidCard;
		this.effectivedate = effectivedate;
		this.utel = utel;
		this.balance = balance;
		this.userId = userId;
		this.startTime = startTime;
		this.upsw = upsw;
	}

	/**
	 * @return uname
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getUname()
	 */
	public String getUname() {
		return uname;
	}

	/**
	 * @param uname
	 *            set uname
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUname(String uname)
	 */
	public void setUname(String uname) {
		this.uname = uname;
	}

	/**
	 * @return uage
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getUage() 
	 */
	public String getUage() {
		return uage;
	}

	/**
	 * @param uage
	 *            set uage
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUage(String uage)
	 */
	public void setUage(String uage) {
		this.uage = uage;
	}

	/**
	 * @return uidCard
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getUidCard() 
	 */
	public String getUidCard() {
		return uidCard;
	}

	/**
	 * @param uidCard
	 *            set uidCard
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUidCard(String uidCard)
	 */
	public void setUidCard(String uidCard) {
		this.uidCard = uidCard;
	}

	/**
	 * @return effectivedate
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getEffectivedate() 
	 */
	public String getEffectivedate() {
		return effectivedate;
	}

	/**
	 * @param effectivedate
	 *            set effectivedate
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setEffectivedate(String effectivedate)
	 */
	public void setEffectivedate(String effectivedate) {
		this.effectivedate = effectivedate;
	}

	/**
	 * @return utel
	 */
	/**
	 * 
	 * @plant.uml UserInfoBean : String getUtel() 
	 */
	public String getUtel() {
		return utel;
	}

	/**
	 * @param utel
	 *            set utel
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUtel(String utel)
	 */
	public void setUtel(String utel) {
		this.utel = utel;
	}

	/**
	 * @return balance
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getBalance()
	 */
	public String getBalance() {
		return balance;
	}

	/**
	 * @param balance
	 *            set balance
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setBalance(String balance)
	 */
	public void setBalance(String balance) {
		this.balance = balance;
	}

	/**
	 * @return userId
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getUserId()
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            set userId
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUserId(String userId) 
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return startTime
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getStartTime()
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            set startTime
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setStartTime(String startTime) 
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return upsw
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : String getUpsw() 
	 */
	public String getUpsw() {
		return upsw;
	}

	/**
	 * @param upsw
	 *            set upsw
	 */
	/**
	 * 
	 * @plant.uml  UserInfoBean : void setUpsw(String upsw)
	 */
	public void setUpsw(String upsw) {
		this.upsw = upsw;
	}
}