package classdiagram;
/**
 * @plant.uml BlackListBean"1" -- "many" UserInfoBean
 * @plant.uml class BlackListBean{
 * @plant.uml String Arreprivate String Arrearageuname;
 * @plant.uml String Arrearageuid;
 * @plant.uml String ArrearageuidCard;
 * @plant.uml String ArrearageBalance;
 * @plant.uml }
 *
 */
public class BlackListBean {
	private String Arrearageuname;
	private String Arrearageuid;
	private String ArrearageuidCard;
	private String ArrearageBalance;
/**
 * 
 * @plant.uml BlackListBean : String getArrearageBalance()
 */
	public String getArrearageBalance() {
		return ArrearageBalance;
	}
/**
 * 
 * @plant.uml BlackListBean : void setArrearageBalance(String arrearageBalance)
 */
	public void setArrearageBalance(String arrearageBalance) {
		ArrearageBalance = arrearageBalance;
	}
/**
 * 
 * @plant.uml BlackListBean : String getArrearageuname()
 */
	public String getArrearageuname() {
		return Arrearageuname;
	}
/**
 * 
 * @plant.uml  BlackListBean : void setArrearageuname(String arrearageuname)
 */
	public void setArrearageuname(String arrearageuname) {
		Arrearageuname = arrearageuname;
	}
/**
 * 
 * @plant.uml BlackListBean : String getArrearageuid()
 */
	public String getArrearageuid() {
		return Arrearageuid;
	}
/**
 * 
 * @plant.uml BlackListBean : void setArrearageuid(String arrearageuid)
 */
	public void setArrearageuid(String arrearageuid) {
		Arrearageuid = arrearageuid;
	}
/**
 * 
 * @plant.uml BlackListBean : String getArrearageuidCard()
 */
	public String getArrearageuidCard() {
		return ArrearageuidCard;
	}
/**
 * 
 * @plant.uml BlackListBean : void setArrearageuidCard(String arrearageuidCard)
 */
	public void setArrearageuidCard(String arrearageuidCard) {
		ArrearageuidCard = arrearageuidCard;
	}
/**
 * 
 * @plant.uml BlackListBean(String Arrearageuname, String Arrearageuid,
			String ArrearageuidCard, String ArrearageBalance)
 */
	public BlackListBean(String Arrearageuname, String Arrearageuid,
			String ArrearageuidCard, String ArrearageBalance) {
		super();
		this.Arrearageuname = Arrearageuname;
		this.ArrearageBalance = ArrearageBalance;
		this.Arrearageuid = Arrearageuid;
		this.ArrearageuidCard = ArrearageuidCard;
	}

	public BlackListBean() {
	}
}
